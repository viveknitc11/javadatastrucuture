package com.ait.arraybasic;

import java.awt.List;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

public class ArrayRotation {

	public void arrayRotationFirstApproach(int arr[])
	{
		int temp,i=0,j;
		temp=arr[i];
		for(i=0;i<arr.length-1;i++)
		{
			arr[i]=arr[i+1];
		}
		arr[i]=temp;
		System.out.println(Arrays.toString(arr));
		
	}
	public void arrayRotationSecondApproach(int arr[],int d)
	{ 	
		int [] temp=new int[d];
		int len=arr.length;
		for(int i=0;i<d;i++)
		{
			temp[i]=arr[i];
		}
		System.out.println(Arrays.toString(temp));
		for(int j=0;j<len-d;j++)
		{
			arr[j]=arr[j+2];
		}
		System.out.println(Arrays.toString(arr));
		int dec=d;
		
		for(int k=0;k<d;k++)
		{
			System.out.println(dec);
			if(dec>0)
			{
			arr[len-dec]=temp[k];
			dec--;
			}
		}
		System.out.println(Arrays.toString(arr));
	}
	public void arrayRotationThirdApproach(int arr[],int start,int end,int order)
	{
		while(start < end)
		{
			//System.out.println(Arrays.toString(arr)+" "+start+" "+ end);
			int temp=arr[start];
			arr[start]=arr[end];
			arr[end]=temp;
			start++;
			end--;
		}
		System.out.println(Arrays.toString(arr));
		
	}
	public void arrayRotationFourthApproach(int arr[],int order)
	{
		for(int i=0;i<order;i++)
		{
			int len=arr.length-1;
			for(int j=0;j<len;j++)
			{
				int temp=arr[j];
				arr[j]=arr[len];
				arr[len]=temp;
			}
		}
		System.out.println(Arrays.toString(arr));
	}
	public void arrayRotationWithJugling(int arr[],int d,int n)
	{
		int i,j,k,temp;
		for(i=0;i<gcd(d,n);i++)
		{
			temp=arr[i];
			j=i;
			while(true)
			{
				k=j+d;
				if(k>=n)
					k=k-n;
				if(k==i)
					break;
				arr[j]=arr[k];
				j=k;
			}
			arr[j]=temp;
		}
		System.out.println(Arrays.toString(arr));
	}
	public int gcd(int a,int b)
	{
		if(b==0)
			return a;
		else
			return gcd(b,a%b);
		
	}
	public static void main(String[] args) {
		
		ArrayRotation arrt=new ArrayRotation();
		int arrayexp[]={1,2,3,4,5,6,7,8,9,10,11,12};
		//arrt.arrayRotationSecondApproach(arrayexp, 2);
		//arrt.arrayRotationThirdApproach(arrayexp, 3);
		//arrt.arrayRotationThirdApproach(arrayexp,0,arrayexp.length-1,3);
		//arrt.arrayRotationFourthApproach(arrayexp, 3);
		arrt.arrayRotationWithJugling(arrayexp, 3, arrayexp.length);
		
		
		
	}

}
