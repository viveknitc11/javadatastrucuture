package com.ait.hashmapexp;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class TestForHashMap {

	public static void main(String[] args) {
	
		HashMap<Employee, Integer> hmap=new HashMap();
		System.out.println(hmap.isEmpty());
		hmap.put(new Employee(143540, "Vivek", 50000), 143540);
		hmap.put(new Employee(143541, "Ujjval", 51000), 143541);
		hmap.put(new Employee(143540, "Vivek", 60000), 143540);
		hmap.put(new Employee(143546, "Amit", 68000), 143546);
		//hmap.put(null, 123456);
		//hmap.put(null, 123676);
		System.out.println(hmap);
		Employee emp=new Employee(143541, "Ujjval", 51000);
		Employee emp1=new Employee(143540, "Vivek", 50000);
		System.out.println(emp);
		System.out.println("Value For Ujjval==>"+hmap.get(emp));
		if(hmap.containsKey(emp1))
		{
			Integer a=hmap.get(emp1);
			System.out.println("Value for Vivek==>"+ a);
		}
		Set<Entry<Employee, Integer>> set=hmap.entrySet();
		System.out.println(set);
		Set<Employee> keyset=hmap.keySet();
		System.out.println(keyset);
		Collection<Integer> valueset=hmap.values();
		System.out.println(valueset);
		Iterator<Employee> itr=keyset.iterator();
		while (itr.hasNext()) {
			Employee employee = (Employee) itr.next();
			//hmap.remove(employee);
			System.out.println(employee);
			System.out.println("==================================");
		}
		Iterator<Map.Entry<Employee, Integer>> itr1=hmap.entrySet().iterator();
		while (itr1.hasNext()) {
			Map.Entry<com.ait.hashmapexp.Employee, java.lang.Integer> entry = (Map.Entry<com.ait.hashmapexp.Employee, java.lang.Integer>) itr1
					.next();
			if(entry.getValue()==143541)
			itr1.remove();
		}
		System.out.println(hmap);
		
		HashMap<Employee, Integer> hmapclone=(HashMap<Employee, Integer>) hmap.clone();
		System.out.println("before using compute method"+hmapclone);
		//hmapclone.compute(new Employee(143546, "Amit", 68000),(key,value) -> null)
		 //System.out.println(hmapclone.compute(new Employee(143542, "Ashit", 80000),(key,value) -> 12345));
		//hmapclone.compute(new Employee(143542, "Ashit", 80000),(key,value) -> 12345);
		//hmapclone.computeIfAbsent(new Employee(143544, "ABC", 123456), (key) -> null);
		//hmapclone.computeIfPresent(new Employee(143540, "Vivek", 50000),(key,value) -> value+9);
		//hmapclone.computeIfPresent(new Employee(143454, "Ashim", 63000),(key,value) -> value+11);
		//hmapclone.computeIfPresent(new Employee(143546, "Amit", 68000),(key,value) -> null);
		//hmapclone.computeIfAbsent(new Employee(143540, "Vivek", 50000),(key) -> 143540+10 );
		//hmapclone.computeIfAbsent(new Employee(143542, "Ashit", 80000),(key) -> 12345);
		//hmapclone.computeIfAbsent(new Employee(143546, "Amit", 68000),(key) -> null);
		//hmapclone.merge(new Employee(143540, "Vivek", 50000), 143540, (key,value) -> 143546+11);
		//hmapclone.merge(new Employee(143546, "Amit", 68000), 143546, (key,value) -> null);
		//hmapclone.merge(new Employee(143542, "Ashit", 80000),143542,(key,value) -> 12345);
		  hmapclone.putIfAbsent(new Employee(143546, "Amit", 68000), 666666);
		System.out.println(hmapclone);
		hmap.clear();
		System.out.println("Clone of Original HashMap"+hmapclone);
		
	}

}
