package com.ait.hashtable;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.print.attribute.SetOfIntegerSyntax;

public class HashTableExpFirst {

	public static void main(String[] args) {
		
		Hashtable<Student, String> htable=new Hashtable<>(2);
		System.out.println(htable.size());
		htable.put(new Student(11, "Vivek", "Second"), "Vivek");
		htable.put(new Student(12, "Anand", "Third"), "Anand");
		htable.put(new Student(13, "prince", "Fifth"), "Prince");
		htable.put(new Student(13, "Amit", "Fifth"), "Prince");
		htable.put(new Student(10, "Amrita", "Fifth"), "Prince");
		htable.put(new Student(10, "Amrita", "Fifth"), "Prince");
		Map<Student,String> hmap=new HashMap<>();
		hmap.put(new Student(20,"Mamta","Tenth"), "Tenth");
		hmap.put(new Student(21,"Bunty","Ninth"), "Ninth");
		htable.putAll(hmap);
		System.out.println(htable.size());
		Set<Entry<Student, String>> hcomb= htable.entrySet();
		System.out.println(hcomb);
		Set<Student> key=htable.keySet();
		Enumeration<Student> henm=htable.keys();
		while (henm.hasMoreElements()) {
			Student student1 = (Student) henm.nextElement();
			System.out.println(student1);
		}
		Iterator<Student> itr=key.iterator();
		while (itr.hasNext()) {
			Student student = (Student) itr.next();
			System.out.println("keys:=="+student+" & value:=="+htable.get(student));
		}
		/*for (Student student : key) {
			System.out.println("keys:=="+student+" & value:=="+htable.get(student));
		}*/
		Set<Student> allkey=htable.keySet();
		System.out.println(allkey);
		Student stu=new Student(20, "Mamta", "Fifth");
		if(htable.containsKey(stu))
		{
		System.out.println(stu+"==>"+htable.get(stu));
		}
		else
		{
		System.out.println("key doesn't available in HashTable");
		}
	}

}

class Student
{
	private int rollNo;
	private String name;
	private String std;
	
	public Student() {
		// TODO Auto-generated constructor stub
	}

	public Student(int rollNo, String name, String std) {
		super();
		this.rollNo = rollNo;
		this.name = name;
		this.std = std;
	}

	public int getRollNo() {
		return rollNo;
	}

	public void setRollNo(int rollNo) {
		this.rollNo = rollNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStd() {
		return std;
	}

	public void setStd(String std) {
		this.std = std;
	}

	@Override
	public int hashCode() {
		/*final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + rollNo;
		result = prime * result + ((std == null) ? 0 : std.hashCode());
		return result;*/
		return (name.hashCode()+rollNo+std.hashCode())/3;
		//return this.getRollNo()/5;
	}

	@Override
	public boolean equals(Object obj) {
		/*if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (rollNo != other.rollNo)
			return false;
		if (std == null) {
			if (other.std != null)
				return false;
		} else if (!std.equals(other.std))
			return false;
		return true;*/
		
		Student st=null;
		if(obj instanceof Student)
		{
			st=(Student)obj;
		}
		if(this.getName()==st.getName() && this.getStd()==st.getStd())
		{
		return true;
		}
		else
		{
		return false;
		}	
	}

	@Override
	public String toString() {
		return "Student [rollNo=" + rollNo + ", name=" + name + ", std=" + std + "]";
	}
	
	
}
