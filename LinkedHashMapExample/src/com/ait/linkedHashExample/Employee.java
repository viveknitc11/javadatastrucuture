package com.ait.linkedHashExample;

public class Employee {

	private int empid;
	private String name;
	private int sal;
	
	public Employee() {
		// TODO Auto-generated constructor stub
	}

	public Employee(int empid, String name, int sal) {
		super();
		this.empid = empid;
		this.name = name;
		this.sal = sal;
	}

	public int getEmpid() {
		return empid;
	}

	public void setEmpid(int empid) {
		this.empid = empid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSal() {
		return sal;
	}

	public void setSal(int sal) {
		this.sal = sal;
	}
 @Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return name.hashCode()+empid+sal/3;
	}
 	
 	public boolean equals(Object obj) {
 		// TODO Auto-generated method stub
 		Employee emp=null;
 		if(obj instanceof Employee)
 		{
 			emp=(Employee)obj;
 		}
 		if(this.getName()==emp.getName() && this.empid==emp.getEmpid())
 		{
 			return true;
 		}
 		else
 		{
 			return false;
 		}
 		
 	}

	@Override
	public String toString() {
		return "Employee [empid=" + empid + ", name=" + name + ", sal=" + sal + "]";
	}
	
	
}
