package com.ait.linkedHashExample;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;


public class TestForLinkedHashMapExample {

	public static void main(String[] args) {
		
		LinkedHashMap<Employee, String> lmap=new LinkedHashMap<>(2,(float).75,false);
		System.out.println(lmap.size());
		lmap.put(new Employee(143540, "Vivek", 40000), "Capgemini");
		lmap.put(new Employee(143541, "Amit", 42000), "Microsoft");
		lmap.put(new Employee(143539, "Ujjval", 39000), "Igate");
		lmap.put(new Employee(143537, "Khushbu", 91000), "CTS");
		System.out.println(lmap.size());
		Employee emp=new Employee(143540, "Vivek", 40000);
		Employee emp1=new Employee(143537, "Khushbu", 91000);
		System.out.println(lmap.get(emp1));
		
		Set<Employee> set1=lmap.keySet();
		/*Iterator<Employee> itr=set1.iterator();
		while (itr.hasNext()) {
			Employee employee = (Employee) itr.next();
			System.out.println(employee);
			lmap.remove(emp);
		}*/
		//Iterate using foreach loop
		for(Employee e:set1)
		{
			System.out.println("Keys:==>"+e+"and Values"+":==>"+lmap.get(e));
		}
		Object lmapp=lmap.clone();
		System.out.println(lmapp);
		/*Iterator<Map.Entry<Employee, String>> itr1=lmap.entrySet().iterator();
		while (itr1.hasNext()) {
			Map.Entry<com.ait.linkedHashExample.Employee, java.lang.String> entry = (Map.Entry<com.ait.linkedHashExample.Employee, java.lang.String>) itr1
					.next();
			if(entry.getValue()=="Igate")
			{
				itr1.remove();
			}
		}*/
		System.out.println(lmap.size()+":==>"+lmap);
		if(lmap.containsValue("CTS"))
		{
			System.out.println("record available in LinkedHashMap");
		}

	}

}
