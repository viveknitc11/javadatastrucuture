package com.ait.lrucacheimplementation;

import java.util.LinkedHashMap;
import java.util.Map;

public class LruCacheImplementation extends LinkedHashMap<String, String> {
	
	private int size;
	private static final long serialVersionUID=-4794975964067719956L;
	public LruCacheImplementation(int size) {
		super(size,0.75F,true);
		this.size=size;
	}

	public static <K,V> LruCacheImplementation newInstance(int size)
	{
		return new LruCacheImplementation(size);
	}
	/*public void setmaxSize(int size)
	{
		this.size=size;
	}*/
	
	@Override
	protected boolean removeEldestEntry(Map.Entry<String, String> elEntry)
	{
		return size()>size;
	}
	public static void main(String[] args) {
		
		LruCacheImplementation lrucache=LruCacheImplementation.newInstance(3);
			lrucache.put("1", "1");
			lrucache.put("2", "2");
			lrucache.put("3", "3");
			lrucache.put("4", "4");
			lrucache.put("1", "1");
			lrucache.put("2", "2");
			lrucache.put("5", "5");
			lrucache.put("1", "1");
			lrucache.put("2", "2");
			lrucache.put("3", "3");
			lrucache.put("4", "4");
			lrucache.put("5", "5");
			System.out.println(lrucache);

	}

}
